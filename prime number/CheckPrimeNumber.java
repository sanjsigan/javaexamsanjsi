import java.util.Scanner;

public class CheckPrimeNumber {
	public static void main(String[] args) {
		int i = 2, number, count = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter Your number: ");
		number = scan.nextInt();
		while (i <= number / 2) {
			if (number % i == 0) {
				count++;
				break;
			}
			i++;
		}
		if (count == 0 & number != 1) {
			System.out.println(number + " is a prime Number");
		} else {
			
			System.out.println(number + " is a Not prime Number");
		}
	}

}
